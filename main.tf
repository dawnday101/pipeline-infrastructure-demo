provider "aws" {}

terraform {
  backend "s3" {
    bucket = "terraform-state"
    key = "development/terraform.tfstate"
    region = "us-east-1"
  }
}

locals {
  service = "demo"
  environment = "development"
  tags = "${merge(map(
    "service", "${local.service}",
    "environment", "${local.environment}"),
    var.tags
  )}"
}

module "network" {
  source = "github.com/kanewinter/tf_modules//network?ref=master"
  tags   = local.tags
}

module "eks" {
  source                         = "github.com/kanewinter/tf_modules//eks?ref=master"
  vpc_id                         = module.network.vpc_id
  eks_subnet_ids                 = module.network.all_subnet_ids
  public_eks_subnet_ids          = module.network.public_subnet_ids
  private_eks_subnet_ids         = module.network.private_subnet_ids
  eks_node_public_instance_type  = var.eks_node_public_instance_type
  eks_node_private_instance_type = var.eks_node_private_instance_type
  eks_node_public_min_size       = var.eks_node_public_min_size
  eks_node_public_max_size       = var.eks_node_public_max_size
  eks_node_private_min_size      = var.eks_node_private_min_size
  eks_node_private_max_size      = var.eks_node_private_max_size
  tags                           = local.tags
}