variable "tags" {
  type = "map"
  default = {
    "organization" = "KaneVentures"
  }
}

variable "region" {
  default = "us-east-1"
}

variable "eks_node_public_instance_type" {
  default = "t3.micro"
}

variable "eks_node_private_instance_type" {
  default = "t3.micro"
}

variable "eks_node_public_min_size" {
  default = "3"
}

variable "eks_node_public_max_size" {
  default = "3"
}

variable "eks_node_private_min_size" {
  default = "0"
}

variable "eks_node_private_max_size" {
  default = "0"
}

variable "cert" {
  default = ""
}

variable "zone_id" {
  default = ""
}