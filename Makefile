makefile_dir 	:= $(abspath $(shell pwd))
SHELL := /bin/bash

ansible_verbose		:= -v
ansible_roles		:= $(makefile_dir)/ansible/roles
ansible_playbooks	:= $(makefile_dir)/ansible/playbooks

.PHONY: init

init:
	. ~/.passwords/awsauth.sh $(acct) $(env); \
	. ~/.passwords/$(acct)-$(env)-pw; \
    cd terraform/$(acct)/environments/$(env); \
	terragrunt init -upgrade

build: #### make build acct=remax env=dev
	. ~/.passwords/awsauth.sh $(acct) $(env); \
    . ~/.passwords/$(acct)-$(env)-pw; \
    cd terraform/$(acct)/environments/$(env); \
    rm -rf .terragrunt* ;\
	terragrunt init ;\
	terragrunt apply -target=module.vpc.aws_vpc.this -target=module.vpc.aws_subnet.public -target=module.vpc.aws_route_table.public ;\
	terragrunt apply -target=module.sg.aws_security_group.this -target=module.ssh_sg.aws_security_group.this -target=module.web_sg.aws_security_group.this -target=module.dev_sg.aws_security_group.this -target=aws_security_group.elasticache_sg -target=module.docker_sg.aws_security_group.this -target=module.eks_sgs.aws_security_group.eks-master

show:
	. ~/.passwords/awsauth.sh $(acct) $(env); \
	. ~/.passwords/$(acct)-$(env)-pw; \
    cd terraform/$(acct)/environments/$(env); \
	terragrunt refresh ;\
	terragrunt show

apply:  #####   make apply acct=remax env=dev
	. ~/.passwords/awsauth.sh $(acct) $(env); \
	. ~/.passwords/$(acct)-$(env)-pw; \
	cd terraform/$(acct)/environments/$(env); \
	rm -rf .terra* ;\
	terragrunt init ;\
	terragrunt refresh ;\
	terragrunt apply

test:
	. ~/.passwords/awsauth.sh $(acct) $(env); \
	. ~/.passwords/$(acct)-$(env)-pw; \
	cd terraform/$(acct)/environments/$(env); \
	terragrunt refresh ;\
	terragrunt validate

ami:
	. ~/.passwords/awsauth.sh $(acct) $(env); \
	. ~/.passwords/$(acct)-$(env)-pw; \
	cd terraform/$(acct)/environments/$(env)/app-ami; \
	terragrunt init ;\
	terragrunt refresh ;\
	terragrunt show ;\
	terragrunt destroy -target=module.appserver-ami.aws_instance.this -auto-approve;\
	terragrunt apply -auto-approve

service:   ####    make service acct=remax env=dev service=medialibrary
	PROMPT_COMMAND='echo -en "\033]0;Make Service $(service) Env $(env)\a"'
	. ~/.passwords/awsauth.sh $(acct) ;\
	cd terraform/$(acct)/environments/$(env)/services/$(service) ;\
	rm -rf .terragrunt-cache ;\
	terragrunt init ;\
	terragrunt apply

service-update:   ####    make service-update acct=remax env=dev service=medialibrary
	. ~/.passwords/awsauth.sh $(acct) ;\
	cd terraform/$(acct)/environments/$(env)/services/$(service) ;\
	rm -rf .terragrunt-cache ;\
	terragrunt 0.12checklist

service-refresh:   ####    make service-refresh acct=remax env=dev service=medialibrary
	. ~/.passwords/awsauth.sh $(acct) ;\
	cd terraform/$(acct)/environments/$(env)/services/$(service) ;\
	rm -rf .terragrunt-cache ;\
	terragrunt refresh

### Environments
environment:   ####    make environment acct=remax environment=ops
	. ~/.passwords/awsauth.sh $(acct) $(environment) ;\
	cd terraform/$(acct)/environments/$(environment) ;\
	rm -rf .terragrunt-cache ;\
	terragrunt init ;\
	terragrunt apply  ;\
	rm -rf .terragrunt-cache

environment-refresh:   ####    make environment-refresh acct=remax environment=ops
	. ~/.passwords/awsauth.sh $(acct) $(environment) ;\
	cd terraform/$(acct)/environments/$(environment) ;\
	rm -rf .terragrunt-cache ;\
	terragrunt refresh ;\
	rm -rf .terragrunt-cache

environment-update:   ####    make environment-update acct=remax environment=ops
	. ~/.passwords/awsauth.sh $(acct) ;\
	cd terraform/$(acct)/environments/$(environment) ;\
	terragrunt 0.12upgrade ;\

destroy-environment:   ####    make destroy-environment acct=remax environment=ops
	. ~/.passwords/awsauth.sh $(acct) $(environment) ;\
	cd terraform/$(acct)/environments/$(environment) ;\
	rm -rf .terragrunt-cache ;\
	terragrunt init ;\
	terragrunt destroy  ;\
	rm -rf .terragrunt-cache
######


build-global:  ###    make build-global acct=remax
	. ~/.passwords/awsauth.sh $(acct); \
	cd terraform/$(acct)/global; \
	terragrunt init -upgrade;\
	terragrunt apply -target=module.vpc ;\
	terragrunt apply

show-global:
	. ~/.passwords/awsauth.sh $(acct); \
	cd terraform/$(acct)/global; \
	terragrunt init ;\
	terragrunt refresh ;\
	terragrunt show

### Deploy Key
### make deploy-key acct=remax env=tester
deploykey:
	sudo docker run --rm -t $(tty &>/dev/null && echo "-i") -e "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" -e "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" -e "AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" \
		-v $(shell pwd):/project \
		xueshanf/awscli aws \
		ec2 create-key-pair --key-name deploy-key-$(env) --query 'KeyMaterial' --output text > ~/.ssh/aws/$(acct)-deploy-key-$(env).pem
	sudo docker run --rm -t $(tty &>/dev/null && echo "-i") -e "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" -e "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" -e "AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" \
		-v $(shell pwd):/project \
		xueshanf/awscli aws \
		secretsmanager create-secret --name deploy-key-$(env) \
		--secret-string "`cat ~/.ssh/aws/$(acct)-deploy-key-$(env).pem`"


######### WARNING: Must set AWS Env variables before SECRETS will work
secret:   ##  make secret acct=remax env=dev service=capitol type=rds
	export PASSWORD=`sudo docker run --rm -t $(tty &>/dev/null && echo "-i") -e "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" -e "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" -e "AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" \
		-v $(shell pwd):/project \
		xueshanf/awscli aws \
		secretsmanager get-random-password --password-length 40 --require-each-included-type --exclude-punctuation | jq -r '.RandomPassword'` ; \
	sudo docker run --rm -t $(tty &>/dev/null && echo "-i") -e "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" -e "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" -e "AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" \
		-v $(shell pwd):/project \
		xueshanf/awscli aws \
		secretsmanager create-secret --name $(service)-$(type)-$(env) \
		--secret-string "$$PASSWORD"

restore-secret:   ##  make restore-secret acct=remax env=dev secret=capitol-dev
	sudo docker run --rm -t $(tty &>/dev/null && echo "-i") -e "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" -e "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" -e "AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" \
		-v $(shell pwd):/project \
		xueshanf/awscli aws \
		secretsmanager restore-secret --secret-id $(secret)

get-secret:   ##  make get-secret acct=remax env=dev secret=capitol-dev
	sudo docker run --rm -t $(tty &>/dev/null && echo "-i") -e "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" -e "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" -e "AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" \
		-v $(shell pwd):/project \
		xueshanf/awscli aws \
		secretsmanager get-secret-value --secret-id $(secret) ####### | jq -r '.SecretString'

###   New Region Layout
rinit:
	. ~/.passwords/awsauth.sh $(acct) $(env); \
	. ~/.passwords/$(acct)-$(env)-pw; \
    cd terraform/$(acct)/$(region)/environments/$(env); \
	terragrunt init

rbuild:
	. ~/.passwords/awsauth.sh $(acct) $(env); \
    . ~/.passwords/$(acct)-$(env)-pw; \
    cd terraform/$(acct)/$(region)/environments/$(env); \
	terragrunt refresh ;\
	terragrunt apply -target=module.sg.aws_security_group.this -target=module.ssh_sg.aws_security_group.this -target=module.web_sg.aws_security_group.this -target=module.dev_sg.aws_security_group.this -target=aws_security_group.elasticache_sg -target=module.docker_sg.aws_security_group.this -target=module.eks_sgs.aws_security_group.eks-master

rshow:
	. ~/.passwords/awsauth.sh $(acct) $(env); \
	. ~/.passwords/$(acct)-$(env)-pw; \
    cd terraform/$(acct)/$(region)/environments/$(env); \
	terragrunt refresh ;\
	terragrunt show

rapply:
	. ~/.passwords/awsauth.sh $(acct) $(env); \
	. ~/.passwords/$(acct)-$(env)-pw; \
	cd terraform/$(acct)/$(region)/environments/$(env); \
	terragrunt init ;\
	terragrunt refresh ;\
	terragrunt apply

build-region:
	. ~/.passwords/awsauth.sh $(acct); \
	cd terraform/$(acct)/$(region)/region; \
	terragrunt init ;\
	terragrunt apply -target=module.vpc-dev -target=module.vpc-prod ;\
	terragrunt refresh ;\
	terragrunt apply

show-region:
	. ~/.passwords/awsauth.sh $(acct); \
	cd terraform/$(acct)/$(region)/region; \
	terragrunt init ;\
	terragrunt refresh ;\
	terragrunt show

###   MasterControl

mastercontrol-config:
	ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_CONFIG=./ansible/ansible.cfg ansible-playbook -i mastercontrol, -u ec2-user --private-key ~/.ssh/aws/ops-prod $(ansible_verbose) $(ansible_playbooks)/ops/environments/prod/mastrctrl.yml

mastercontrol-update:
	ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_CONFIG=./ansible/ansible.cfg ansible-playbook -i mastercontrol, -u ec2-user --private-key ~/.ssh/aws/ops-prod $(ansible_verbose) $(ansible_playbooks)/ops/environments/prod/mastrctrl.yml --tags "update"

dashboard:
	ssh mastercontrol "pkill -f "kubectl proxy" ; kubectl proxy &"

mastercontrol-auth:
	ssh mastercontrol "sudo mkdir -p /tmp/auth/aws"
	ssh mastercontrol "sudo mkdir -p /tmp/auth/ssh"
	ssh mastercontrol "sudo mkdir -p /tmp/auth/passwords"
	scp -3 ansible.lan:/home/booj_ansible/.ssh/aws/* mastercontrol:/tmp/auth/aws/
	scp -3 ansible.lan:/home/booj_ansible/.ssh/mastercontrol* mastercontrol:/tmp/auth/ssh/
	scp -3 ansible.lan:/home/booj_ansible/.passwords/* mastercontrol:/tmp/auth/passwords/



###    EKS
eks-deploykube:
	. ~/.passwords/awsauth.sh $(acct); \
	cd terraform/$(acct)/global; \
	terragrunt output kubeconfig > ../../../kubernetes/$(acct)-eksconfig ;\
	export KUBECONFIG=~/sysconfigs/kubernetes/$(acct)-eksconfig ; \
	cat ~/sysconfigs/kubernetes/$(acct)-eksconfig ; \
	terragrunt output config-map-aws-auth > ~/sysconfigs/kubernetes/$(acct)-config-map-aws-auth ; \
	cd ../../..
	kubectl apply -f kubernetes/$(acct)-config-map-aws-auth
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
	kubectl -n kube-system patch deploy heapster -p '{"spec": {"template": {"spec": {"containers":[{"name": "heapster","command":["/heapster","--source=kubernetes.summary_api:https://kubernetes.default.svc?kubeletHttps=true&kubeletPort=10250&useServiceAccount=true&insecure=true","--sink=influxdb:http://monitoring-influxdb.kube-system.svc:8086"]}]}}}}'
	kubectl apply -f kubernetes/eks-admin-service-account.yaml
	kubectl apply -f kubernetes/eks-admin-cluster-role-binding.yaml
	kubectl apply -f kubernetes/dashboard.yaml
	kubectl create clusterrolebinding permissive-binding \
	--clusterrole=cluster-admin \
	--user=eks-admin \
	--user=kubelet \
	--group=system:serviceaccounts
	. ~/.passwords/gitlab \
	kubectl create secret docker-registry gitlab-registry \
	  --namespace=default \
	  --docker-server=registry.gitlab.com \
	  --docker-username=gitlab-ci@booj.com \
	  --docker-password=$GITLABPW \
	  --docker-email=gitlab-ci@booj.com
	make eks-getkubetoken | grep -A10 default-token* | grep token\:
	kubectl get --all-namespaces all
	make eks-setenv acct=$(acct)


# 1
eks-getkubeconfig:
	. ~/.passwords/awsauth.sh $(acct); \
	cd terraform/$(acct)/global; \
	terragrunt output kubeconfig > ../../../kubernetes/$(acct)-eksconfig ;\
	export KUBECONFIG=~/sysconfigs/kubernetes/$(acct)-eksconfig ; \
	kubectl get --all-namespaces all
	make eks-setenv acct=$(acct)

# 2
eks-setupworkernodes:
	. ~/.passwords/awsauth.sh $(acct); \
	cd terraform/$(acct)/global; \
	terragrunt output config-map-aws-auth > ~/sysconfigs/kubernetes/$(acct)-config-map-aws-auth ; \
	cd ../../..
	kubectl apply -f kubernetes/$(acct)-config-map-aws-auth
	kubectl get nodes

# 3
eks-deploykubeservices:
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
	kubectl apply -f kubernetes/eks-admin-service-account.yaml
	kubectl apply -f kubernetes/eks-admin-cluster-role-binding.yaml
	#kubectl apply -f kubernetes/dns.yaml
	kubectl apply -f kubernetes/dashboard.yaml
	kubectl create clusterrolebinding permissive-binding \
	--clusterrole=cluster-admin \
	--user=eks-admin \
	--user=kubelet \
	--group=system:serviceaccounts
	. ~/.passwords/gitlab \
	kubectl create secret docker-registry gitlab-registry \
	  --namespace=default \
	  --docker-server=registry.gitlab.com \
	  --docker-username=gitlab-ci@booj.com \
	  --docker-password=$GITLABPW \
	  --docker-email=gitlab-ci@booj.com

# 4
eks-fluentd:
	kubectl apply -f kubernetes/fluentd/aws-secret.yaml
	kubectl apply -f kubernetes/fluentd/fluentd-configmap.yaml
	kubectl apply -f kubernetes/fluentd/fluentd-ds.yaml

eks-setenv:
	@printf "Run this: \n . ~/.passwords/awsauth.sh $(acct) \n export KUBECONFIG=./kubernetes/$(acct)-eksconfig \n kubectl get --all-namespaces all \n"

eks-setupkubeauth:
	wget -O /usr/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
	go get -u -v github.com/kubernetes-sigs/aws-iam-authenticator/cmd/aws-iam-authenticator

eks-getkubetoken:
	kubectl -n kube-system describe secret $(kubectl -n default get secret | grep eks-admin | awk '{print $1}')

eks-geteksadmintoken:
	make eks-getkubetoken | grep -A10 eks-admin | grep token\:

eks-dashboard:
	if [[ ! -z `ps aux | grep "kubectl proxy" | grep -v grep` ]] ; then pkill -f "kubectl proxy" ; fi
	. ~/.passwords/awsauth.sh booj ;\
	export KUBECONFIG=./kubernetes/booj-eksconfig ;\
	kubectl proxy -p 29090 &
	. ~/.passwords/awsauth.sh remax ;\
	export KUBECONFIG=./kubernetes/remax-eksconfig ;\
	kubectl proxy -p 28080 &
	echo "Booj on 29090 Remax on 28080"

httpauth:
	htpasswd -b -c /tmp/auth $(user) $(password) ; kubectl -n $(namespace) create secret generic $(secret) --from-file /tmp/auth




unlock:
	@echo " "
	@echo "####################################################"
	@echo "enter: terraform force-unlock LOCK_ID [WORKING DIR]"
	@echo "####################################################"
	@echo " "

remove:
	@echo " "
	@echo "####################################################"
	@echo "enter: terraform force-unlock LOCK_ID [WORKING DIR]"
	@echo "####################################################"
	@echo " "


