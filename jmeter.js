var support_ui = JavaImporter(org.openqa.selenium.support.ui.WebDriverWait);
var pkg = JavaImporter(org.openqa.selenium);
var excon = JavaImporter(org.openqa.selenium.support.ui)
var wait = new support_ui.WebDriverWait(WDS.browser, 50);
var vars = org.apache.jmeter.threads.JMeterContextService.getContext().getVariables()
var thread = JavaImporter(java.lang)
var actions = JavaImporter(org.openqa.selenium.interactions)
var action = new actions.Actions(WDS.browser)



try {
   WDS.sampleResult.sampleStart()
   WDS.browser.get('https://app.piplanning.io/#/page/login')
   WDS.browser.findElement(pkg.By.xpath("/html/body/div[2]/div/div[3]/div/div[2]/div/form/div[1]/input")).sendKeys(vars.get('COMPANY'))
   WDS.browser.findElement(pkg.By.xpath("/html/body/div[2]/div/div[3]/div/div[2]/div/form/div[2]/input")).sendKeys(vars.get('USER'))
   WDS.browser.findElement(pkg.By.xpath("/html/body/div[2]/div/div[3]/div/div[2]/div/form/div[3]/input")).sendKeys(vars.get("PASSWORD"))
   WDS.browser.findElement(pkg.By.xpath("/html/body/div[2]/div/div[3]/div/div[2]/div/form/div[4]/button")).click()
   wait.until(excon.ExpectedConditions.elementToBeClickable(pkg.By.xpath("/html/body/div[2]/div[2]/div[4]/div/div[2]/div[2]"))).click()
   wait.until(excon.ExpectedConditions.urlContains("team"))
   wait.until(excon.ExpectedConditions.elementToBeClickable(pkg.By.xpath("/html/body/div[2]/div[2]/div[4]/div/div[2]/div[2]"))).click()
	var from = wait.until(excon.ExpectedConditions.visibilityOfElementLocated(pkg.By.id('5e727d530236cbb2c82fb65e')))
	var to = WDS.browser.findElement(pkg.By.xpath("//*[@id='5e712ca33675f311b1c4a935']/div[1]/div[5]"))
   action.dragAndDrop(from, to).build().perform()
   WDS.sampleResult.sampleEnd()  
}
catch (exc) {
	WDS.log.error(exc)
	WDS.sampleResult.setSuccessful(false)
	WDS.sampleResult.setResponseMessage("took to long to respond")
}
